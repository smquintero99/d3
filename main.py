from PIL import Image
import numpy as np
import os

path = '../Static'

files = []
for r, d, f in os.walk(path):
    for file in f:
        if '.png' in file:
            files.append(os.path.join(r, file))

fire = [149, 1,	147]
terra = [177, 97, 72]
air = [21, 40, 178]
water = [5, 77, 27]

ordered_colors = [
    air,
    fire,
    water,
    terra,
    air,
    fire,
    air,
    water,
    fire,
    water,
    terra,
    terra
]

names = [
    'Aqu',
    'Ari',
    'Can',
    'Cap',
    'Gem',
    'Leo',
    'Lib',
    'Pis',
    'Sag',
    'Sco',
    'Tau',
    'Vir'
]

def change_color(idx):
    im = Image.open(files[idx])
    data = np.array(im)
    print(data)

    color = ordered_colors[idx]

    r1, g1, b1 = 0, 0, 0
    r2, g2, b2 = color[0], color[1], color[2]

    red, green, blue = data[:,:,0], data[:,:,1], data[:,:,2]
    mask = (red == r1) & (green == g1) & (blue == b1)
    data[:,:,:3][mask] = [r2, g2, b2]

    print(red, green, blue)
    print(mask)

    im = Image.fromarray(data)
    im.save(names[idx] + '.png')

def change_b_w_color(idx):

    im = Image.open(files[idx])
    data = np.array(im)
    print(data.shape)
    z = np.zeros((353, 276, 2), dtype=data.dtype)

    new_data = np.c_[z, data]
    color = ordered_colors[idx]

    r1, g1, b1 = 0, 0, 0
    r2, g2, b2 = color[0], color[1], color[2]

    red, green, blue = new_data[:,:,0], new_data[:,:,1], new_data[:,:,2]
    mask = (red == r1) & (green == g1) & (blue == b1)
    new_data[:,:,:3][mask] = [r2, g2, b2]

    im = Image.fromarray(new_data)
    im.save(names[idx] + '.png')


change_b_w_color(5)
