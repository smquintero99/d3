const swisseph = require ('swisseph')
const { year, month, day, hour } = { year: 1988, month: 8, day: 18, hour: 0.62 }
const jul_day = swisseph.swe_julday(year, month, day, hour, swisseph.SE_GREG_CAL)
const flag = swisseph.SEFLG_SPEED

swisseph.swe_set_ephe_path (__dirname + '/../ephe')
const { SE_SUN: SUN, SE_MOON: MOON, SE_MERCURY: MERCURY, SE_VENUS: VENUS, SE_MARS: MARS, SE_JUPITER: JUPITER } = swisseph
const { SE_SATURN: SATURN, SE_URANUS: URANUS, SE_NEPTUNE: NEPTUNE, SE_PLUTO: PLUTO, SE_TRUE_NODE: NODE } = swisseph
const planets = [ SUN, MOON, MERCURY, VENUS, MARS, JUPITER, SATURN, URANUS, NEPTUNE, PLUTO, NODE ]

const get_position = planet => swisseph.swe_calc_ut(jul_day, planet, flag, ({ longitude }) => console.log(longitude))
planets.map(get_position)

const { house, ascendant } = swisseph.swe_houses(jul_day, 19.4326, -99.1332, 'P')
const signs = [
    'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagitarius', 'Capricorn', 'Aquarius', 'Pisces'
]

console.log(ascendant)
console.log(house)
house.map(h => console.log(h % 30, signs[Math.floor(h/30)]))
console.log(house.map(h => h % 30)[0])

