import swisseph from 'swisseph'
import express from 'express'
import cors from 'cors'

swisseph.swe_set_ephe_path (__dirname + '/../ephe')

const { year, month, day, hour } = { year: 1988, month: 8, day: 18, hour: 0.62 }
const jul_day = swisseph.swe_julday(year, month, day, hour, swisseph.SE_GREG_CAL)
const flag = swisseph.SEFLG_SPEED

const { SE_SUN: SUN, SE_MOON: MOON, SE_MERCURY: MERCURY, SE_VENUS: VENUS, SE_MARS: MARS, SE_JUPITER: JUPITER } = swisseph
const { SE_SATURN: SATURN, SE_URANUS: URANUS, SE_NEPTUNE: NEPTUNE, SE_PLUTO: PLUTO, SE_TRUE_NODE: NODE } = swisseph
const planets = [ SUN, MOON, MERCURY, VENUS, MARS, JUPITER, SATURN, URANUS, NEPTUNE, PLUTO, NODE ]
const { house } = swisseph.swe_houses(jul_day, 19.4326, -99.1332, 'P')

const get_position = planet => swisseph.swe_calc_ut(jul_day, planet, flag, ({ longitude }) => longitude)

const app = express()
app.use(cors())
app.get('/', (_, res)  => res.send('Hello world'))
app.get('/san', (_, res) => res.send({
    ascendant: house[0] % 30,
    planets: [...planets.map(planet => get_position(planet).longitude).map(planet => [
        Math.ceil(planet/30), 
        planet % 30, 
        `${Math.floor(planet % 30)}° ${
            String(Math.round((planet % 1)*60)/100).slice(2).length > 1
            ? String(Math.round((planet % 1)*60)/100).slice(2)
            : String(Math.round((planet % 1)*60)/100).slice(2) + 0
        }'`
    ]),
    [
        Math.ceil((get_position(planets[10]).longitude+180)%360/30), 
        (get_position(planets[10]).longitude+180) % 360 % 30, 
        `${Math.floor((get_position(planets[10]).longitude+180) % 30)}° ${
            String(Math.round((get_position(planets[10]).longitude % 1)*60)/100).slice(2).length > 1
            ? String(Math.round((get_position(planets[10]).longitude % 1)*60)/100).slice(2)
            : String(Math.round((get_position(planets[10]).longitude % 1)*60)/100).slice(2) + 0
        }'`]
    ],
    houses: house
}))

app.listen(process.env.PORT || 4000)
