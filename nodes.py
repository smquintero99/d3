from PIL import Image
import numpy as np
import os

path = '../Static/Nodes'

files = []
for r, d, f in os.walk(path):
    for file in f:
        if '.png' in file:
            files.append(os.path.join(r, file))

fire = [149, 1,	147]
terra = [177, 97, 72]
air = [21, 40, 178]
water = [5, 77, 27]

ordered_colors = [ air, fire, water, terra ]
color_names =[ 'air', 'fire', 'water', 'terra' ]
names = [ 'Node' ]

def change_b_w_color(idx, color_idx):

    im = Image.open(files[idx])
    data = np.array(im)
    z = np.zeros((data.shape[0], data.shape[1], 4), dtype=data.dtype)

    new_data = np.c_[data]
    color = ordered_colors[color_idx]
    r1, g1, b1 = 0, 0, 0
    r2, g2, b2 = color[0], color[1], color[2]

    red, green, blue = new_data[:,:,0], new_data[:,:,1], new_data[:,:,2]
    mask = (red == r1) & (green == g1) & (blue == b1)
    new_data[:,:,:3][mask] = [r2, g2, b2]

    im = Image.fromarray(new_data)
    im.save(names[idx] + '_' + color_names[color_idx] + '.png')

for (i, f) in enumerate(files):
    for (j, c) in enumerate(color_names):
        try: change_b_w_color(i, j)
        except Exception as e: print(i, f, e)
